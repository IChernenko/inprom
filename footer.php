<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package inprom
 */

?>
<footer class="footer" id="footer">
    <div class="footer-contacts">
        <div class="footer-logo">
            <?php the_custom_logo(); ?>
        </div>
        <?php
        $copyright = carbon_get_theme_option( 'crb_footer_copyright' );
        $email = carbon_get_theme_option( 'crb_footer_email' );
        $tel = carbon_get_theme_option( 'crb_footer_tel' );

        // output the field value

        ?>
        <div class="footer-adress">
           <?php echo $copyright; ?>
        </div>
        <div class="footer-phone-email">
            <?php echo nl2br($tel) ?>
            <p class="footer-email">E-mail: <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></p>
        </div>
        <div class="footer-developer">

        </div>
    </div>
    <div class="footer-menu">
        <?php
        wp_nav_menu(array(
            'theme_location' => 'menu-1',
            'container' => false,
            'items_wrap' => '<ul class="wow">%3$s</ul>',
        ));
        ?>
    </div>
</footer>


<div class="overlay" id="overlay"></div>
<div id="popup" class="popup">
    <div class="popup-close" id="popup-close">&#215</div>
    <h3>Оставить заявку</h3>
    <?php echo do_shortcode('[contact-form-7 id="93" title="Попап форма"]'); ?>
</div>


<?php wp_footer(); ?>
<script>new WOW().init();</script>
</body>
</html>
