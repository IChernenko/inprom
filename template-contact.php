<?php
/**
 * Template Name: Contact
 */

get_header();
?>

    <div class="section-contacts">
    <div class="container">
        <div class="contact-list">
            <div class="contact-list__item">
                <p class="contact-city">г. Усть-Каменогорск</p>
                <div class="contact-item-list">
                    <div class="contact-group">
                        <div class="contact-item">
                            <div class="contact-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/contact-icon/adress.png' ?>"
                                     alt="">
                            </div>
                            <p><b>Адрес</b></p>
                            <p>070000, ВКО, г. Усть-Каменогорск ул. Казахстан 158-3</p>
                        </div>
                        <div class="contact-item">
                            <div class="contact-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/contact-icon/tel.png' ?>"
                                     alt="">
                            </div>
                            <p><b>Телефон</b></p>
                            <a class="contact-item__link" href="tel:+7 7232 40-17-40">+7 7232 40-17-40</a>
                        </div>
                    </div>
                    <div class="contact-group">
                        <div class="contact-item">
                            <p><b>Отдел Трубопроводных систем</b></p>
                            <p>Жакаев Серикбек</p>
                        </div>
                        <div class="contact-item">
                            <div class="contact-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/contact-icon/tel.png' ?>"
                                     alt="">
                            </div>
                            <p><b>Телефон</b></p>
                            <a class="contact-item__link" href="tel:+7 701 498-37-85">+7 701 498-37-85</a>
                        </div>
                        <div class="contact-item">
                            <div class="contact-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/contact-icon/mail.png' ?>"
                                     alt="">
                            </div>
                            <p><b>E-mail</b></p>
                            <a class="contact-item__link" href="mailto:market@inprom.kz">market@inprom.kz</a>
                        </div>


                    </div>

                    <div class="contact-group">
                        <div class="contact-item">
                            <p><b>Отдел Строительной химии</b></p>
                        </div>
                        <div class="contact-item">
                            <div class="contact-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/contact-icon/tel.png' ?>"
                                     alt="">
                            </div>
                            <p><b>Телефон</b></p>
                            <a class="contact-item__link" href="tel:+7 701 514-10-11">+7 701 514-10-11</a>
                        </div>
                        <div class="contact-item">
                            <div class="contact-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/contact-icon/mail.png' ?>"
                                     alt="">
                            </div>
                            <p><b>E-mail</b></p>
                            <a class="contact-item__link" href="mailto:info@inprom.kz">info@inprom.kz</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-list__item">
                <p class="contact-city">г. Нур-Султан</p>
                <div class="contact-item-list">
                    <div class="contact-group">
                        <div class="contact-item">
                            <div class="contact-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/contact-icon/adress.png' ?>"
                                     alt="">
                            </div>
                            <p><b>Адрес</b></p>
                            <p>г. Нур-Султан, микрорайон Самал, д. 2</p>
                        </div>
                    </div>
                    <div class="contact-group">
                        <div class="contact-item">
                            <p><b>Отдел Строительной химии</b></p>
                            <p>Сагнаев Талгат</p>
                        </div>
                        <div class="contact-item">
                            <div class="contact-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/contact-icon/tel.png' ?>"
                                     alt="">
                            </div>
                            <p><b>Телефон</b></p>
                            <a class="contact-item__link" href="tel:+7 (701) 485 76 40">+7 (701) 485 76 40</a>
                        </div>
                        <div class="contact-item">
                            <div class="contact-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/contact-icon/mail.png' ?>"
                                     alt="">
                            </div>
                            <p><b>E-mail</b></p>
                            <a class="contact-item__link" href="mailto:t.sagnayev@inprom.kz">t.sagnayev@inprom.kz</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--        <a target="_blank"-->
        <!--           href="https://yandex.ru/maps/?um=constructor:QdZdkKJx1aJJ5-hfEsRNKtjZmv6KNQcw&amp;source=constructorStatic"-->
        <!--           class="mobile-map">-->
        <!--            <img style="border: 0;" src="-->
        <?php //echo get_stylesheet_directory_uri() . '/assets/img/map.png' ?><!--" alt="">-->
        <!--        </a>-->
        <div class="contact-form">
            <?php echo do_shortcode('[contact-form-7 id="107" title="Контактная форма"]'); ?>
        </div>
        <!--        <div id="map" class="map">-->
        <!--            <div style="position: absolute; width: 90%; height: 600px; z-index: 9; top: 25px; left: 5%;">-->
        <!--                <iframe style="height: 100%;" src="https://yandex.ru/map-widget/v1/?um=constructor%3A7f4896a396bfb4466b8e4d8de2cbf5a5b390fcdb1284f3a97c067439cf3ba2bd&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>-->
        <!--            </div>-->
        <!--        </div>-->
    </div>
<?php
get_footer();
