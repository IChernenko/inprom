<?php
/**
 * Template Name: About
 */

get_header();
?>

    <div class="window-activity">
    <div class="section-activity">
        <div class="activity-image">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/img-dosie.png' ?> " alt="">
            <div class="activity-start wow fadeInUp" data-wow-delay=".6s">

                <p class="activity-start__year">2008</p>

                <p>

                    Начало активной деятельности

                    компании ИНПРОМ

                </p>

            </div>
        </div>
        <div class="activity-box-container">

            <?php
            $about_page_complex = carbon_get_post_meta($about_page->ID, 'inprom_about_text_blocks');
            foreach ($about_page_complex as $data) {
                ?>
                <div class="activity-box">
                    <h3 class="activity-box__title"><?php echo $data['title'] ?></h3>
                    <?php echo $data['text'] ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="section-work">
        <h2>РАБОТАЯ С НАМИ ВЫ ПОЛУЧАЕТЕ</h2>

        <div>

            <?php
            $about_page_complex = carbon_get_post_meta($about_page->ID, 'inprom_about_complex');
            foreach ($about_page_complex as $data) {


                ?>

                <div class="box-img-reliability-wrap">
                    <div class="box-img-reliability wow fadeInLeft" data-wow-delay=".3s">
                        <img class="box-image"
                             src="<?php echo $data['image'] ?>" alt="">
                    </div>

                    <div class="box-text wow fadeInRight" data-wow-delay=".6s">
                        <h4 class="box-text__title"><?php echo $data['title'] ?></h4>
                        <?php echo $data['text'] ?>
                    </div>
                </div>

            <?php } ?>

        </div>

        <div class="box-img-benefit-850">
            <img class="box-image" src="<?php echo get_stylesheet_directory_uri() . '/assets/img/box-benefit.png' ?>"
                 alt="">
        </div>

    </div>

    <div class="section section-about-form" id="section3">

        <div class="offer">

            <div class="offer-title">
                <h2>ОСТАВЬТЕ ЗАЯВКУ ПРЯМО СЕЙЧАС <br>
                    И ПОЛУЧИТЕ КОНСУЛЬТАЦИЮ <br>
                    ТЕХНИЧЕСКОГО СПЕЦИАЛИСТА
                </h2>
            </div>
            <div class="offer-form">
                <?php echo do_shortcode('[contact-form-7 id="92" title="Главная форма"]'); ?>
            </div>
        </div>


    </div>

<?php
get_footer();
