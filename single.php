<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package inprom
 */

get_header();
?>

	<div class="section-article">

		<?php
		while ( have_posts() ) :
			the_post(); ?>

            <div class="new-article">
                <div class="in-row-1">
                    <div class="in-date">
                        <div class="art-date-1"><?php echo get_the_date('d'); ?></div>
                        <div class="art-date-2"><?php echo get_the_date('F'); ?></div>
                        <div class="art-date-3"><?php echo get_the_date('Y'); ?></div>
                    </div>
                </div>
                <div class="in-row-2">
                    <div class="in-title">
                        <p><?php the_title(); ?></p>
                    </div>
                    <div class="in-text">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>

		<?php endwhile; // End of the loop.
		?>

    </div>

<?php

get_footer();
