<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package inprom
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="header index-header">

    <?php
        $src = '';
        $title = '';
        $text = '';
        if ( is_page_template('template-main.php') ) {
            global $main_page;
            $main_page = get_pages(array(
                'meta_key' => '_wp_page_template',
                'meta_value' => 'template-main.php',
                'hierarchical' => 0
            ))[0];
            $src = carbon_get_post_meta($main_page->ID, 'inprom_header_image');
            $title = carbon_get_post_meta($main_page->ID, 'inprom_header_title');
            $text = carbon_get_post_meta($main_page->ID, 'inprom_header_text');
        }

        if ( is_page_template('template-about.php') ) {
            global $about_page;
            $about_page = get_pages(array(
                'meta_key' => '_wp_page_template',
                'meta_value' => 'template-about.php',
                'hierarchical' => 0
            ))[0];
            $src = carbon_get_post_meta($about_page->ID, 'inprom_header_image');
            $title = carbon_get_post_meta($about_page->ID, 'inprom_header_title');
            $text = carbon_get_post_meta($about_page->ID, 'inprom_header_text');
        }

        if ( is_page_template('template-type_chemistry.php') ) {
            global $chemistry_page;
            $chemistry_page = get_pages(array(
                'meta_key' => '_wp_page_template',
                'meta_value' => 'template-type_chemistry.php',
                'hierarchical' => 0
            ))[0];
            $src = carbon_get_post_meta($chemistry_page->ID, 'inprom_header_image');
            $title = carbon_get_post_meta($chemistry_page->ID, 'inprom_header_title');
            $text = carbon_get_post_meta($chemistry_page->ID, 'inprom_header_text');
        }

        if ( is_page_template('template-type_system.php') ) {
            global $system_page;
            $system_page = get_pages(array(
                'meta_key' => '_wp_page_template',
                'meta_value' => 'template-type_system.php',
                'hierarchical' => 0
            ))[0];
            $src = carbon_get_post_meta($system_page->ID, 'inprom_header_image');
            $title = carbon_get_post_meta($system_page->ID, 'inprom_header_title');
            $text = carbon_get_post_meta($system_page->ID, 'inprom_header_text');
        }

        if ( is_page_template('template-articles.php') ) {
            global $articles_page;
            $articles_page = get_pages(array(
                'meta_key' => '_wp_page_template',
                'meta_value' => 'template-articles.php',
                'hierarchical' => 0
            ))[0];
            $src = carbon_get_post_meta($articles_page->ID, 'inprom_header_image');
            $title = carbon_get_post_meta($articles_page->ID, 'inprom_header_title');
            $text = carbon_get_post_meta($articles_page->ID, 'inprom_header_text');
        }

        if ( is_page_template('template-contact.php') ) {
            global $contact_page;
            $contact_page = get_pages(array(
                'meta_key' => '_wp_page_template',
                'meta_value' => 'template-contact.php',
                'hierarchical' => 0
            ))[0];
            $src = carbon_get_post_meta($contact_page->ID, 'inprom_header_image');
            $title = carbon_get_post_meta($contact_page->ID, 'inprom_header_title');
            $text = carbon_get_post_meta($contact_page->ID, 'inprom_header_text');
        }

        if ( is_single() ) {
            $src = get_the_post_thumbnail_url();
        }


        if ( $src == '' ) {
            $scr = get_template_directory_uri() . '/assets/img/header-bg.png';
        }
    ?>

    <img class="header-bg" src="<?php echo $src ?>" alt="main">

    <div class="header-logo">
        <?php the_custom_logo(); ?>
    </div>
    <div class="header-menu">
        <?php
        wp_nav_menu(array(
            'theme_location' => 'menu-1',
            'container' => false,
            'items_wrap' => '<ul class="wow">%3$s</ul>',
        ));
        ?>

    </div>
    <h1 class="wow fadeInUp" style="visibility: hidden"><?php echo nl2br($title)  ?></h1>

    <?php
        if ( $text != '' ) { ?>
            <div class="wow fadeInUp header__description" style="visibility: hidden"><?php echo $text ?></div>
     <?php   }
    ?>


    <?php
        if ( (!$about_page) && (!$chemistry_page) && (!$system_page) && (!$articles_page) ) { ?>
            <span class="header-btn btn btn--white wow fadeInUp">Оставить заявку</span>
    <?php    }
    ?>


    <div id="menutoggle" class="menutoggle">
        <div id="nav-icon3">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <?php
        $copyright = carbon_get_theme_option( 'crb_footer_copyright' );
        $email = carbon_get_theme_option( 'crb_footer_email' );
        $tel = carbon_get_theme_option( 'crb_footer_tel' );
    ?>

    <div class="header-phone-adress__mobile">
<!--        <a class="header-phone__mobile-link" href="tel:+--><?php //echo $tel ?><!--">--><?php //echo $tel ?><!--</a>-->
        <?php echo $copyright ?>
    </div>
</header>
