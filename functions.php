<?php
/**
 * inprom functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package inprom
 */

if ( ! function_exists( 'inprom_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function inprom_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on inprom, use a find and replace
		 * to change 'inprom' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'inprom', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'inprom' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'inprom_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'inprom_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function inprom_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'inprom_content_width', 640 );
}
add_action( 'after_setup_theme', 'inprom_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function inprom_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'inprom' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'inprom' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'inprom_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function inprom_scripts() {
    wp_enqueue_style( 'inprom-style', get_stylesheet_uri() );

    wp_enqueue_style('inprom-fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css');
    wp_enqueue_style('inprom-animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css');
    wp_enqueue_style('inprom-normalize', get_template_directory_uri() . '/assets/style/normolize.css');
    wp_enqueue_style('inprom-main-css', get_template_directory_uri() . '/assets/style/style.css');
    wp_enqueue_style('inprom-main-media', get_template_directory_uri() . '/assets/style/media.css');
    wp_enqueue_style('inprom-burger-media', get_template_directory_uri() . '/assets/style/burger.css');
    wp_enqueue_style('inprom-fontawesome', 'https://use.fontawesome.com/releases/v5.6.3/css/all.css');



    wp_enqueue_script('inprom-fontawesome-js', 'https://use.fontawesome.com/releases/v5.6.3/js/all.js', array(), 457, true);
    wp_enqueue_script('inprom-jquery-js', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), 456, true);
    wp_enqueue_script('inprom-wow-js', 'https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js', array(), 458, true);
    wp_enqueue_script('inprom-fancybox-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js', array(), 4516, true);


    wp_enqueue_script('inprom-scrollSpeed-js', get_template_directory_uri() . '/assets/js/jQuery.scrollSpeed.js', array(), 1617318, true);
    wp_enqueue_script('inprom-scripts-js', get_template_directory_uri() . '/assets/js/scripts.js', array(), 1611718, true);



}
add_action( 'wp_enqueue_scripts', 'inprom_scripts' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';



add_action('carbon_fields_register_fields', 'cotti_add_theme_data');

function cotti_add_theme_data()
{
//    require get_template_directory() . '/inc/post-types/models.php';
//    require get_template_directory() . '/inc/post-types/about.php';
    require get_template_directory() . '/inc/post-types/page.php';
}

