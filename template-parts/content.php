<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package inprom
 */

?>

<div class="section-articles">
    <?php

    $args = array(
        'posts_per_page' => -1,
        'post_type' => 'post'
    );
    $loop = new WP_Query($args);

    while ($loop->have_posts()) : $loop->the_post(); ?>

        <div class="article" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="article-wrap">
                <img class="article-image" src="<?php echo get_the_post_thumbnail_url() ?>" alt="image">
                <div class="art-img-hover"></div>
                <div class="art-date">
                    <div class="art-date-1"><?php echo get_the_date('d'); ?></div>
                    <div class="art-date-2"><?php echo get_the_date('F'); ?></div>
                    <div class="art-date-3"><?php echo get_the_date('Y'); ?></div>
                </div>
            </div>
            <div class="art-text">
                <h1><?php echo get_the_title() ?></h1>
                <?php echo get_the_content() ?>
                <div class="art-more"><a href="<?php the_permalink(); ?>">
                        <button class="button-order">ПОДРОБНЕЕ</button>
                    </a></div>
            </div>
        </div>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); ?>
</div>

