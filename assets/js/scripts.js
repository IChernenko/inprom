$(document).ready(function () {


    /**
     * Scroll speed
     */
    // $.scrollSpeed(200, 1400);


    /**
     * Mobile second click
     */
    var touchHover = function () {
        $('[data-hover]').click(function (e) {
            e.preventDefault();
            var $this = $(this);
            var onHover = $this.attr('data-hover');
            var linkHref = $this.attr('href');
            if (linkHref && $this.hasClass(onHover)) {
                location.href = linkHref;
                return false;
            }
            $this.toggleClass(onHover);
        });
    };

    touchHover();


    /**
     * Burger menu
     */
    $('#nav-icon3').click(function () {
        $(this).toggleClass('open');
    });


    /**
     * Mobile menu
     */

    $('.menutoggle').click(function () {
        $(this).toggleClass('open');
        if ($('.header-menu').css("right") == '-200px') {
            $('.header-menu').animate({right: "0"}, 600);
        } else {
            $('.header-menu').animate({right: "-200"}, 600);
        }
    });


    /**
     * Show/hide popup
     */
    $('.header-btn, .tabs-inform__btn').click(function () {
        centerBox();
        $('.overlay').fadeIn(500);
        $('.popup').fadeIn(500);
        // $('html').css('overflow', 'hidden');
    });

    $('.overlay, .popup-close').click(function () {
        $('.overlay').fadeOut(500);
        $('.popup').fadeOut(500);
        // $('html').css('overflow', 'auto');
    });


    /**
     * Tab & Select
     */
    $('.tabs__caption-main').each(function () {
        $(this).find('.tabs__caption-btn').each(function (i) {
            $(this).click(function () {
                $(this).addClass('active').siblings().removeClass('active')
                    .closest('div.tabs').find('div.tabs__content').removeClass('active').eq(i).addClass('active');
            });
        });
    });

    $('.tabs__caption-mobile').change(function () {
        var i = $(this).val();
        $(this).addClass('active').siblings().removeClass('active').closest('div.tabs').find('div.tabs__content').removeClass('active').eq(i).addClass('active');
    });


    /**
     * Popup center for vertical
     */
    function centerBox() {

        var winHeight = $(document).height();
        var scrollPos = $(window).scrollTop();
        var disHeight = scrollPos + 100;
        $('.popup').css({'top': disHeight + 'px'});

        return false;
    }

    $('.field-item input').on('focus', function () {
        $(this).closest('.field-item').addClass('focus')
    });
    $('.field-item input').on('focusout', function () {
        if ($(this).val() == '') {
            $(this).closest('.field-item').removeClass('focus');
        }
    });

    $(window).resize(centerBox);
    centerBox();

});

$(window).on('load', function () {
//     alert(1)

    if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
        $('.loader_inner').fadeOut('500');
    }
    else {
        $('.loader_inner').addClass('animated flipOutY');
    }


    $('.loader').css('-webkit-animation', 'loader 1s linear');
    $('.loader').css('animation', 'loader 1s linear');

    // $("html, body").animate({scrollTop: 0}, 1200);

    setTimeout(
        function () {
            $(".loader-right").addClass('animated slideOutRight');
            $(".loader-left").addClass('animated slideOutLeft');
        }, 800);
});


