<?php
/**
 * Template Name: main page
 */

get_header();
?>
    <div class="index-container-1" id="index-container-1">
        <div class="chemistry-pipelines-container">
            <div class="chemistry-content chemistry-item">
                <img src="<?php echo carbon_get_post_meta($main_page->ID, 'sec_left_image') ?>" alt="image">
                <div class="chemistry-item__group">
                    <h3><?php echo carbon_get_post_meta($main_page->ID, 'sec_left_title') ?></h3>
                    <?php echo carbon_get_post_meta($main_page->ID, 'sec_left_text') ?>
                </div>
                <div class="chemistry-item__group">
                    <a href="<?php echo carbon_get_post_meta($main_page->ID, 'sec_left_link') ?>"
                       class="chemistry-btn btn btn--white">Перейти к продукции</a>
                </div>
            </div>
            <div class="pipelines-content chemistry-item">
                <img src="<?php echo carbon_get_post_meta($main_page->ID, 'sec_right_image') ?>" alt="">
                <div class="chemistry-item__group">
                    <h3><?php echo carbon_get_post_meta($main_page->ID, 'sec_right_title') ?></h3>
                    <?php echo carbon_get_post_meta($main_page->ID, 'sec_right_text') ?>
                </div>
                <div class="chemistry-item__group">
                    <a href="<?php echo carbon_get_post_meta($main_page->ID, 'sec_right_link') ?>"
                       class="pipelines-btn btn btn--white">Перейти к продукции</a>
                </div>
            </div>
        </div>
    </div>

    <div class="index-advantages">
        <img class="index-advantages__image"
             src="<?php echo get_stylesheet_directory_uri() . '/assets/img/advantage.png' ?>" alt="">
        <h2 class="wow fadeInUp"
            data-wow-delay=".3s"><?php echo carbon_get_post_meta($main_page->ID, 'third_block_title') ?></h2>


        <div class="advantages-content wow fadeInUp">

            <?php
            $adv_complex = carbon_get_post_meta($main_page->ID, 'inprom_main_complex');


            foreach ($adv_complex as $data) { ?>
                <div class="advantages-block">
                    <div class="advantages-block__photo"><img src="<?php echo $data['type_nested_image'] ?>"
                                                              alt="section"></div>
                    <?php echo $data['inprom_main_title'] ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="index-form" id="index-form">
        <h2>Получите выгодное предложение</h2>
        <!--        <form>-->
        <!--            <div class="form-inputs-textarea form-item-wrap">-->
        <!--                <div class="form-inputs form-item">-->
        <!--                    <input type="text" name="name" placeholder="Ваше имя">-->
        <!--                    <input type="text" name="company" placeholder="Компания">-->
        <!--                    <input type="tel" name="phone" placeholder="Телефон">-->
        <!--                    <input type="email" name="email" placeholder="E-mail">-->
        <!--                    <input type="text" name="products" placeholder="Интересующая продукция">-->
        <!--                </div>-->
        <!--                <div class="form-textarea form-item">-->
        <!--                    <textarea placeholder="Сообщение"></textarea>-->
        <!--                    <input class="btn index-form-btn btn--gray" type="submit" name="send" value="Отправить сообщение">-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </form>-->
        <?php echo do_shortcode('[contact-form-7 id="92" title="Главная форма"]'); ?>
    </div>


<?php
get_footer(); ?>

<div class="loader-right"></div>
<div class="loader-left"></div>
<div class="loader_inner"></div>
<div class="loader"></div>


