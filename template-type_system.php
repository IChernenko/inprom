<?php
/**
 * Template Name: system
 */


$type_page = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-type_system.php',
    'hierarchical' => 0
))[0];

get_header();
?>

<?php
$type_page_meta = carbon_get_post_meta($type_page->ID, 'inprom_type_complex');
?>

    <div class="chemistry-tabs tabs" id="chemistry-tabs">
        <div class="tabs__caption tabs__caption-main">
            <?php
            $active = 'active';
            $i = 0;
            foreach ($type_page_meta as $data) {
                if ( $i > 0 ) {
                    $active = '';
                }
                $i++;
                ?>
                <div class="tabs__caption-btn <?php echo $active ?>"><span><?php echo $data['inprom_type_title'] ?></span></div>
            <?php } ?>
        </div>

        <select class="tabs__caption tabs__caption-mobile">
            <?php
            $active = 'active';
            $i = 0;
            foreach ($type_page_meta as $data) {
                if ( $i > 0 ) {
                    $active = '';
                }
                $i++;
                ?>
                <option value="<?php echo $i ?>" class="tabs__caption-btn <?php echo $active ?>">
                    <?php echo $data['inprom_type_title'] ?>
                </option>
            <?php } ?>
        </select>


        <div class="tabs-container">
            <?php
            $active = 'active';
            $i = 0;
            foreach ($type_page_meta as $data) {
                if ( $i > 0 ) {
                    $active = '';
                }
                $i++;
                ?>

                <div class="tabs__content <?php echo $active ?>">
                    <?php foreach ($data['inprom_type_nested'] as $nested_data) {

                        ?>
                        <div class="tab-block">
                            <div class="tabs__content-inform">
                                <div class="tabs__content-inform--text">
                                   <?php echo $nested_data['type_nested_text'] ?>
                                </div>
                                <span class="tabs-inform__btn btn btn--white">Заказать</span>
                            </div>
                            <div class="tabs__content-photo">
                                <img src=" <?php echo $nested_data['type_nested_image'] ?>" alt="dobavki">
                            </div>
                        </div>
                    <?php } ?>


                </div>


            <?php } ?>

        </div>







    </div>


<?php
get_footer();
