<?php



use Carbon_Fields\Container;
use Carbon_Fields\Field;


Container::make('post_meta', 'General Block')
    ->show_on_post_type('page')
    ->add_fields( array(
        Field::make( 'image', 'inprom_header_image', 'Изображение' )->set_value_type( 'url' )->set_width( 50 ),
        Field::make( 'textarea', 'inprom_header_title', 'Заголовок' )->set_width( 50 ),
        Field::make( 'rich_text', 'inprom_header_text', 'Описание' )->set_width( 100 )

    ));

Container::make('post_meta', 'Data')
    ->show_on_template(array('template-type_chemistry.php', 'template-type_system.php'))
    ->show_on_post_type('page')
    ->add_fields( array(
            Field::make( 'file', 'inprom_crb_file', __( 'File' ) )->set_value_type( 'url' ),
            Field::make( 'complex', 'inprom_type_complex' )->set_layout( 'tabbed-horizontal' )
                ->add_fields( array(
                    Field::make( 'text', 'inprom_type_title' ),
                    Field::make( 'complex', 'inprom_type_nested' )->set_layout( 'tabbed-horizontal' )
                        ->add_fields( array(
                            Field::make( 'image', 'type_nested_image' )->set_width( 20 )->set_value_type( 'url' ),
                            Field::make( 'rich_text', 'type_nested_text' )->set_width( 80 ),
                        ))
                )),
    ));


Container::make('post_meta', 'Data')
    ->show_on_template('template-main.php')
    ->show_on_post_type('page')
//    ->add_tab( 'Первый блок', array(
//        Field::make( 'image', 'first_block_image', 'Изображение' )->set_width( 10 )->set_value_type( 'url' ),
//        Field::make( 'textarea', 'first_block_title', 'Заголовок' )->set_width( 10 )
//    ))
    ->add_tab( 'Второй блок', array(
        Field::make( 'image', 'sec_left_image', 'Блок слева изображение' )->set_width( 15 )->set_value_type( 'url' ),
        Field::make( 'textarea', 'sec_left_title', 'Блок слева заголовок' )->set_width( 50 ),
        Field::make( 'text', 'sec_left_link', 'Ссылка' )->set_width( 35 ),
        Field::make( 'rich_text', 'sec_left_text', 'Блок слева текст' )->set_width( 100 ),

        Field::make( 'image', 'sec_right_image', 'Блок справа изображение' )->set_width( 15 )->set_value_type( 'url' ),
        Field::make( 'textarea', 'sec_right_title', 'Блок справа заголовок' )->set_width( 50 ),
        Field::make( 'text', 'sec_right_link', 'Ссылка' )->set_width( 35 ),
        Field::make( 'rich_text', 'sec_right_text', 'Блок справа текст' )->set_width( 100 ),
    ))
    ->add_tab( 'Третий блок', array(
        Field::make( 'image', 'third_block_image' )->set_width( 10 )->set_value_type( 'url' ),
        Field::make( 'textarea', 'third_block_title' )->set_width( 10 ),
        Field::make( 'complex', 'inprom_main_complex', 'Inprom ' )->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'rich_text', 'inprom_main_title', '' ),
                Field::make( 'image', 'type_nested_image' )->set_width( 10 )->set_value_type( 'url' ),
            )),
    ));




Container::make('post_meta', 'About company')
    ->show_on_post_type('page')
    ->where( 'post_template', '=', 'template-about.php' )
    ->add_fields( array(
        Field::make( 'complex', 'inprom_about_text_blocks' )->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'text', 'title', '' ),
                Field::make( 'rich_text', 'text', '' ),
            )),

        Field::make( 'complex', 'inprom_about_complex' )->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'image', 'image', 'Изображение' )->set_value_type( 'url' )->set_width( 50 ),
                Field::make( 'text', 'title', 'Заголовок' )->set_width( 50 ),
                Field::make( 'rich_text', 'text', 'Описание' )->set_width( 100 )
            ))


    ));

Container::make( 'theme_options', 'Footer' )
    ->add_fields( array(
        Field::make( 'rich_text', 'crb_footer_copyright', 'Text' ),
        Field::make( 'rich_text', 'crb_footer_tel', 'Телефон' ),
        Field::make( 'text', 'crb_footer_email', 'E-mail' ),
    ) );



